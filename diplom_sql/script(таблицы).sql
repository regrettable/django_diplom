USE [Obmen_E2]
GO
/****** Object:  Table [dbo].[VP01]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01](
	[id_pr] [int] IDENTITY(1,1) NOT NULL,
	[N_KA] [int] NULL,
	[DSF] [datetime] NULL,
	[N_RP] [int] NULL,
	[RNF] [int] NULL,
	[data_zap] [datetime] NULL,
	[form_id] [int] NULL,
	[k] [smallint] NULL,
	[m] [smallint] NULL,
	[r] [smallint] NULL,
	[d] [smallint] NULL,
	[s] [smallint] NULL,
	[t] [smallint] NULL,
	[g] [smallint] NULL,
	[p] [smallint] NULL,
	[Date_N_RP] [datetime] NULL,
	[Date_K_RP] [datetime] NULL,
	[UD] [tinyint] NULL,
	[SynchFlag] [bit] NOT NULL,
	[SynchFlagARMUTS] [bit] NOT NULL,
	[SynchFlagZS] [bit] NOT NULL,
	[flagGotovn] [bit] NULL,
 CONSTRAINT [PK_VP01] PRIMARY KEY CLUSTERED 
(
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_MCI]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_MCI](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_mci] [smallint] NULL,
	[num_ppi1] [smallint] NULL,
	[num_ppi2] [smallint] NULL,
	[vid_inf] [tinyint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[delta] [int] NULL,
 CONSTRAINT [PK_VP01_MCI] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_MSU]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_MSU](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_msu] [smallint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[complectMSU1] [tinyint] NULL,
	[spectraVD11] [tinyint] NULL,
	[spectraVD12] [tinyint] NULL,
	[spectraVD13] [tinyint] NULL,
	[spectraIK14] [tinyint] NULL,
	[spectraIK15] [tinyint] NULL,
	[spectraIK16] [tinyint] NULL,
	[spectraIK17] [tinyint] NULL,
	[spectraIK18] [tinyint] NULL,
	[spectraIK19] [tinyint] NULL,
	[spectraIK110] [tinyint] NULL,
	[complectMSU2] [tinyint] NULL,
	[spectraVD21] [tinyint] NULL,
	[spectraVD22] [tinyint] NULL,
	[spectraVD23] [tinyint] NULL,
	[spectraIK24] [tinyint] NULL,
	[spectraIK25] [tinyint] NULL,
	[spectraIK26] [tinyint] NULL,
	[spectraIK27] [tinyint] NULL,
	[spectraIK28] [tinyint] NULL,
	[spectraIK29] [tinyint] NULL,
	[spectraIK210] [tinyint] NULL,
	[num_ppi2] [smallint] NULL,
	[typeSeans] [tinyint] NULL,
	[durationSeans] [smallint] NULL,
 CONSTRAINT [PK_VP01_MSU333] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_OMI]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_OMI](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_omi] [smallint] NULL,
	[vidOMI] [tinyint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[num_ppi1] [smallint] NULL,
	[dlit_OMI] [int] NULL,
 CONSTRAINT [PK_VP01_OMI] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_ONA]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_ONA](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_ustONA] [smallint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[num_ONA] [smallint] NULL,
	[num_ppi] [smallint] NULL,
	[dlit] [int] NULL,
 CONSTRAINT [PK_VP01_ONA] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_TehnS]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_TehnS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_TehnS] [smallint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[complectMSU1] [tinyint] NULL,
	[spectraVD11] [tinyint] NULL,
	[spectraVD12] [tinyint] NULL,
	[spectraVD13] [tinyint] NULL,
	[spectraIK14] [tinyint] NULL,
	[spectraIK15] [tinyint] NULL,
	[spectraIK16] [tinyint] NULL,
	[spectraIK17] [tinyint] NULL,
	[spectraIK18] [tinyint] NULL,
	[spectraIK19] [tinyint] NULL,
	[spectraIK110] [tinyint] NULL,
	[complectMSU2] [tinyint] NULL,
	[spectraVD21] [tinyint] NULL,
	[spectraVD22] [tinyint] NULL,
	[spectraVD23] [tinyint] NULL,
	[spectraIK24] [tinyint] NULL,
	[spectraIK25] [tinyint] NULL,
	[spectraIK26] [tinyint] NULL,
	[spectraIK27] [tinyint] NULL,
	[spectraIK28] [tinyint] NULL,
	[spectraIK29] [tinyint] NULL,
	[spectraIK210] [tinyint] NULL,
	[num_ppi2] [smallint] NULL,
	[dlit] [int] NULL,
 CONSTRAINT [PK_VP01_Tehn_S1] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_TNP]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_TNP](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_TNP] [smallint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[num_ppi2] [smallint] NULL,
	[durationTestSign] [smallint] NULL,
 CONSTRAINT [PK_VP01_TNP] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_UstMSU]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_UstMSU](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_UstMSU] [smallint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[date_N] [datetime] NULL,
	[date_K] [datetime] NULL,
	[complectMSU] [tinyint] NULL,
	[spectraVD1] [tinyint] NULL,
	[spectraVD2] [tinyint] NULL,
	[spectraVD3] [tinyint] NULL,
	[spectraIK4] [tinyint] NULL,
	[spectraIK5] [tinyint] NULL,
	[spectraIK6] [tinyint] NULL,
	[spectraIK7] [tinyint] NULL,
	[spectraIK8] [tinyint] NULL,
	[spectraIK9] [tinyint] NULL,
	[spectraIK10] [tinyint] NULL,
	[nombe_ppi2] [smallint] NULL,
	[dlit] [int] NULL,
 CONSTRAINT [PK_VP01_UstMSU123] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VP01_VD]    Script Date: 02.04.2018 9:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VP01_VD](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_pr] [int] NOT NULL,
	[num_KalibrVD] [smallint] NULL,
	[dateNach] [datetime] NULL,
	[dateCon] [datetime] NULL,
	[complectMSU] [tinyint] NULL,
	[num_ppi] [smallint] NULL,
	[dlit] [int] NULL,
 CONSTRAINT [PK_VP01_VD333] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_pr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[VP01] ADD  CONSTRAINT [DF_VP01_UD]  DEFAULT ((0)) FOR [UD]
GO
ALTER TABLE [dbo].[VP01] ADD  CONSTRAINT [DF_VP01_SynchFlag]  DEFAULT ((0)) FOR [SynchFlag]
GO
ALTER TABLE [dbo].[VP01] ADD  CONSTRAINT [DF_VP01_SynchFlagARMUTS]  DEFAULT ((0)) FOR [SynchFlagARMUTS]
GO
ALTER TABLE [dbo].[VP01] ADD  CONSTRAINT [DF_VP01_SynchFlagZS]  DEFAULT ((0)) FOR [SynchFlagZS]
GO
ALTER TABLE [dbo].[VP01] ADD  CONSTRAINT [DF_VP01_flagGotovn]  DEFAULT ((0)) FOR [flagGotovn]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_complectMSU1]  DEFAULT ((0)) FOR [complectMSU1]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraVD11]  DEFAULT ((0)) FOR [spectraVD11]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraVD12]  DEFAULT ((0)) FOR [spectraVD12]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraVD13]  DEFAULT ((0)) FOR [spectraVD13]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK14]  DEFAULT ((0)) FOR [spectraIK14]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK15]  DEFAULT ((0)) FOR [spectraIK15]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK16]  DEFAULT ((0)) FOR [spectraIK16]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK17]  DEFAULT ((0)) FOR [spectraIK17]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK18]  DEFAULT ((0)) FOR [spectraIK18]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK19]  DEFAULT ((0)) FOR [spectraIK19]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK110]  DEFAULT ((0)) FOR [spectraIK110]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_complectMSU2]  DEFAULT ((0)) FOR [complectMSU2]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraVD21]  DEFAULT ((0)) FOR [spectraVD21]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraVD22]  DEFAULT ((0)) FOR [spectraVD22]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraVD23]  DEFAULT ((0)) FOR [spectraVD23]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK24]  DEFAULT ((0)) FOR [spectraIK24]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK25]  DEFAULT ((0)) FOR [spectraIK25]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK26]  DEFAULT ((0)) FOR [spectraIK26]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK27]  DEFAULT ((0)) FOR [spectraIK27]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK28]  DEFAULT ((0)) FOR [spectraIK28]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK29]  DEFAULT ((0)) FOR [spectraIK29]
GO
ALTER TABLE [dbo].[VP01_MSU] ADD  CONSTRAINT [DF_VP01_MSU333_spectraIK210]  DEFAULT ((0)) FOR [spectraIK210]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_complectMSU1]  DEFAULT ((0)) FOR [complectMSU1]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraVD11]  DEFAULT ((0)) FOR [spectraVD11]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraVD12]  DEFAULT ((0)) FOR [spectraVD12]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraVD13]  DEFAULT ((0)) FOR [spectraVD13]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK14]  DEFAULT ((0)) FOR [spectraIK14]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK15]  DEFAULT ((0)) FOR [spectraIK15]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK16]  DEFAULT ((0)) FOR [spectraIK16]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK17]  DEFAULT ((0)) FOR [spectraIK17]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK18]  DEFAULT ((0)) FOR [spectraIK18]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK19]  DEFAULT ((0)) FOR [spectraIK19]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK110]  DEFAULT ((0)) FOR [spectraIK110]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_complectMSU2]  DEFAULT ((0)) FOR [complectMSU2]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraVD21]  DEFAULT ((0)) FOR [spectraVD21]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraVD22]  DEFAULT ((0)) FOR [spectraVD22]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraVD23]  DEFAULT ((0)) FOR [spectraVD23]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK24]  DEFAULT ((0)) FOR [spectraIK24]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK25]  DEFAULT ((0)) FOR [spectraIK25]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK26]  DEFAULT ((0)) FOR [spectraIK26]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK27]  DEFAULT ((0)) FOR [spectraIK27]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK28]  DEFAULT ((0)) FOR [spectraIK28]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK29]  DEFAULT ((0)) FOR [spectraIK29]
GO
ALTER TABLE [dbo].[VP01_TehnS] ADD  CONSTRAINT [DF_VP01_Tehn_S1_spectraIK210]  DEFAULT ((0)) FOR [spectraIK210]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_complectMSU]  DEFAULT ((0)) FOR [complectMSU]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraVD1]  DEFAULT ((0)) FOR [spectraVD1]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraVD2]  DEFAULT ((0)) FOR [spectraVD2]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraVD3]  DEFAULT ((0)) FOR [spectraVD3]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK4]  DEFAULT ((0)) FOR [spectraIK4]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK5]  DEFAULT ((0)) FOR [spectraIK5]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK6]  DEFAULT ((0)) FOR [spectraIK6]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK7]  DEFAULT ((0)) FOR [spectraIK7]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK8]  DEFAULT ((0)) FOR [spectraIK8]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK9]  DEFAULT ((0)) FOR [spectraIK9]
GO
ALTER TABLE [dbo].[VP01_UstMSU] ADD  CONSTRAINT [DF_VP01_UstMSU123_spectraIK10]  DEFAULT ((0)) FOR [spectraIK10]
GO
ALTER TABLE [dbo].[VP01_MCI]  WITH CHECK ADD  CONSTRAINT [FK_VP01_MCI_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VP01_MCI] CHECK CONSTRAINT [FK_VP01_MCI_VP01]
GO
ALTER TABLE [dbo].[VP01_MSU]  WITH CHECK ADD  CONSTRAINT [FK_VP01_MSU_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
GO
ALTER TABLE [dbo].[VP01_MSU] CHECK CONSTRAINT [FK_VP01_MSU_VP01]
GO
ALTER TABLE [dbo].[VP01_OMI]  WITH CHECK ADD  CONSTRAINT [FK_VP01_OMI_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VP01_OMI] CHECK CONSTRAINT [FK_VP01_OMI_VP01]
GO
ALTER TABLE [dbo].[VP01_ONA]  WITH CHECK ADD  CONSTRAINT [FK_VP01_ONA_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VP01_ONA] CHECK CONSTRAINT [FK_VP01_ONA_VP01]
GO
ALTER TABLE [dbo].[VP01_TehnS]  WITH CHECK ADD  CONSTRAINT [FK_VP01_Tehn_S_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
GO
ALTER TABLE [dbo].[VP01_TehnS] CHECK CONSTRAINT [FK_VP01_Tehn_S_VP01]
GO
ALTER TABLE [dbo].[VP01_TNP]  WITH CHECK ADD  CONSTRAINT [FK_VP01_TNP_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VP01_TNP] CHECK CONSTRAINT [FK_VP01_TNP_VP01]
GO
ALTER TABLE [dbo].[VP01_UstMSU]  WITH CHECK ADD  CONSTRAINT [FK_VP01_UstMSU_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
GO
ALTER TABLE [dbo].[VP01_UstMSU] CHECK CONSTRAINT [FK_VP01_UstMSU_VP01]
GO
ALTER TABLE [dbo].[VP01_VD]  WITH CHECK ADD  CONSTRAINT [FK_VP01_VD_VP01] FOREIGN KEY([id_pr])
REFERENCES [dbo].[VP01] ([id_pr])
GO
ALTER TABLE [dbo].[VP01_VD] CHECK CONSTRAINT [FK_VP01_VD_VP01]
GO
