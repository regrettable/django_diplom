from django import template
from satellite_production.views import STD_TIME_FORMAT

register = template.Library()


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()
    dict_[field] = value
    return dict_.urlencode()


# Преобразование даты из формата отображения в таблице в стандартный формат
@register.simple_tag()
def tabledisp_to_std(v):
    return v.strftime(STD_TIME_FORMAT), v.strftime("%H%M")

