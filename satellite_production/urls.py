from .views import *
from django.urls import path

app_name = 'satellite_production'
urlpatterns = [
    path(r'tables', MultitableView.as_view(), name='multiple_tables_view'),
]