from django.apps import AppConfig


class SatelliteProductionConfig(AppConfig):
    name = 'satellite_production'
