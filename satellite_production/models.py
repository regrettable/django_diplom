# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Vp01(models.Model):
    id_pr = models.AutoField(primary_key=True)
    n_ka = models.IntegerField(db_column='N_KA', blank=True, null=True)  # Field name made lowercase.
    dsf = models.DateTimeField(db_column='DSF', blank=True, null=True)  # Field name made lowercase.
    n_rp = models.IntegerField(db_column='N_RP', blank=True, null=True)  # Field name made lowercase.
    rnf = models.IntegerField(db_column='RNF', blank=True, null=True)  # Field name made lowercase.
    data_zap = models.DateTimeField(blank=True, null=True)
    form_id = models.IntegerField(blank=True, null=True)
    k = models.SmallIntegerField(blank=True, null=True)
    m = models.SmallIntegerField(blank=True, null=True)
    r = models.SmallIntegerField(blank=True, null=True)
    d = models.SmallIntegerField(blank=True, null=True)
    s = models.SmallIntegerField(blank=True, null=True)
    t = models.SmallIntegerField(blank=True, null=True)
    g = models.SmallIntegerField(blank=True, null=True)
    p = models.SmallIntegerField(blank=True, null=True)
    date_n_rp = models.DateTimeField(db_column='Date_N_RP', blank=True, null=True)  # Field name made lowercase.
    date_k_rp = models.DateTimeField(db_column='Date_K_RP', blank=True, null=True)  # Field name made lowercase.
    ud = models.SmallIntegerField(db_column='UD', blank=True, null=True)  # Field name made lowercase.
    synchflag = models.BooleanField(db_column='SynchFlag')  # Field name made lowercase.
    synchflagarmuts = models.BooleanField(db_column='SynchFlagARMUTS')  # Field name made lowercase.
    synchflagzs = models.BooleanField(db_column='SynchFlagZS')  # Field name made lowercase.
    flaggotovn = models.NullBooleanField(db_column='flagGotovn')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VP01'


class Vp01Mci(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_mci = models.SmallIntegerField(blank=True, null=True)
    num_ppi1 = models.SmallIntegerField(blank=True, null=True)
    num_ppi2 = models.SmallIntegerField(blank=True, null=True)
    vid_inf = models.SmallIntegerField(blank=True, null=True)
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    delta = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VP01_MCI'
        unique_together = (('id', 'id_pr'),)


class Vp01Msu(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_msu = models.SmallIntegerField(blank=True, null=True)
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    complectmsu1 = models.SmallIntegerField(db_column='complectMSU1', blank=True, null=True)  # Field name made lowercase.
    spectravd11 = models.SmallIntegerField(db_column='spectraVD11', blank=True, null=True)  # Field name made lowercase.
    spectravd12 = models.SmallIntegerField(db_column='spectraVD12', blank=True, null=True)  # Field name made lowercase.
    spectravd13 = models.SmallIntegerField(db_column='spectraVD13', blank=True, null=True)  # Field name made lowercase.
    spectraik14 = models.SmallIntegerField(db_column='spectraIK14', blank=True, null=True)  # Field name made lowercase.
    spectraik15 = models.SmallIntegerField(db_column='spectraIK15', blank=True, null=True)  # Field name made lowercase.
    spectraik16 = models.SmallIntegerField(db_column='spectraIK16', blank=True, null=True)  # Field name made lowercase.
    spectraik17 = models.SmallIntegerField(db_column='spectraIK17', blank=True, null=True)  # Field name made lowercase.
    spectraik18 = models.SmallIntegerField(db_column='spectraIK18', blank=True, null=True)  # Field name made lowercase.
    spectraik19 = models.SmallIntegerField(db_column='spectraIK19', blank=True, null=True)  # Field name made lowercase.
    spectraik110 = models.SmallIntegerField(db_column='spectraIK110', blank=True, null=True)  # Field name made lowercase.
    complectmsu2 = models.SmallIntegerField(db_column='complectMSU2', blank=True, null=True)  # Field name made lowercase.
    spectravd21 = models.SmallIntegerField(db_column='spectraVD21', blank=True, null=True)  # Field name made lowercase.
    spectravd22 = models.SmallIntegerField(db_column='spectraVD22', blank=True, null=True)  # Field name made lowercase.
    spectravd23 = models.SmallIntegerField(db_column='spectraVD23', blank=True, null=True)  # Field name made lowercase.
    spectraik24 = models.SmallIntegerField(db_column='spectraIK24', blank=True, null=True)  # Field name made lowercase.
    spectraik25 = models.SmallIntegerField(db_column='spectraIK25', blank=True, null=True)  # Field name made lowercase.
    spectraik26 = models.SmallIntegerField(db_column='spectraIK26', blank=True, null=True)  # Field name made lowercase.
    spectraik27 = models.SmallIntegerField(db_column='spectraIK27', blank=True, null=True)  # Field name made lowercase.
    spectraik28 = models.SmallIntegerField(db_column='spectraIK28', blank=True, null=True)  # Field name made lowercase.
    spectraik29 = models.SmallIntegerField(db_column='spectraIK29', blank=True, null=True)  # Field name made lowercase.
    spectraik210 = models.SmallIntegerField(db_column='spectraIK210', blank=True, null=True)  # Field name made lowercase.
    num_ppi2 = models.SmallIntegerField(blank=True, null=True)
    typeseans = models.SmallIntegerField(db_column='typeSeans', blank=True, null=True)  # Field name made lowercase.
    durationseans = models.SmallIntegerField(db_column='durationSeans', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VP01_MSU'
        unique_together = (('id', 'id_pr'),)


class Vp01Omi(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_omi = models.SmallIntegerField(blank=True, null=True)
    vidomi = models.SmallIntegerField(db_column='vidOMI', blank=True, null=True)  # Field name made lowercase.
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    num_ppi1 = models.SmallIntegerField(blank=True, null=True)
    dlit_omi = models.IntegerField(db_column='dlit_OMI', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VP01_OMI'
        unique_together = (('id', 'id_pr'),)


class Vp01Ona(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_ustona = models.SmallIntegerField(db_column='num_ustONA', blank=True, null=True)  # Field name made lowercase.
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    num_ona = models.SmallIntegerField(db_column='num_ONA', blank=True, null=True)  # Field name made lowercase.
    num_ppi = models.SmallIntegerField(blank=True, null=True)
    dlit = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VP01_ONA'
        unique_together = (('id', 'id_pr'),)


class Vp01Tnp(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_tnp = models.SmallIntegerField(db_column='num_TNP', blank=True, null=True)  # Field name made lowercase.
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    num_ppi2 = models.SmallIntegerField(blank=True, null=True)
    durationtestsign = models.SmallIntegerField(db_column='durationTestSign', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VP01_TNP'
        unique_together = (('id', 'id_pr'),)


class Vp01Tehns(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_tehns = models.SmallIntegerField(db_column='num_TehnS', blank=True, null=True)  # Field name made lowercase.
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    complectmsu1 = models.SmallIntegerField(db_column='complectMSU1', blank=True, null=True)  # Field name made lowercase.
    spectravd11 = models.SmallIntegerField(db_column='spectraVD11', blank=True, null=True)  # Field name made lowercase.
    spectravd12 = models.SmallIntegerField(db_column='spectraVD12', blank=True, null=True)  # Field name made lowercase.
    spectravd13 = models.SmallIntegerField(db_column='spectraVD13', blank=True, null=True)  # Field name made lowercase.
    spectraik14 = models.SmallIntegerField(db_column='spectraIK14', blank=True, null=True)  # Field name made lowercase.
    spectraik15 = models.SmallIntegerField(db_column='spectraIK15', blank=True, null=True)  # Field name made lowercase.
    spectraik16 = models.SmallIntegerField(db_column='spectraIK16', blank=True, null=True)  # Field name made lowercase.
    spectraik17 = models.SmallIntegerField(db_column='spectraIK17', blank=True, null=True)  # Field name made lowercase.
    spectraik18 = models.SmallIntegerField(db_column='spectraIK18', blank=True, null=True)  # Field name made lowercase.
    spectraik19 = models.SmallIntegerField(db_column='spectraIK19', blank=True, null=True)  # Field name made lowercase.
    spectraik110 = models.SmallIntegerField(db_column='spectraIK110', blank=True, null=True)  # Field name made lowercase.
    complectmsu2 = models.SmallIntegerField(db_column='complectMSU2', blank=True, null=True)  # Field name made lowercase.
    spectravd21 = models.SmallIntegerField(db_column='spectraVD21', blank=True, null=True)  # Field name made lowercase.
    spectravd22 = models.SmallIntegerField(db_column='spectraVD22', blank=True, null=True)  # Field name made lowercase.
    spectravd23 = models.SmallIntegerField(db_column='spectraVD23', blank=True, null=True)  # Field name made lowercase.
    spectraik24 = models.SmallIntegerField(db_column='spectraIK24', blank=True, null=True)  # Field name made lowercase.
    spectraik25 = models.SmallIntegerField(db_column='spectraIK25', blank=True, null=True)  # Field name made lowercase.
    spectraik26 = models.SmallIntegerField(db_column='spectraIK26', blank=True, null=True)  # Field name made lowercase.
    spectraik27 = models.SmallIntegerField(db_column='spectraIK27', blank=True, null=True)  # Field name made lowercase.
    spectraik28 = models.SmallIntegerField(db_column='spectraIK28', blank=True, null=True)  # Field name made lowercase.
    spectraik29 = models.SmallIntegerField(db_column='spectraIK29', blank=True, null=True)  # Field name made lowercase.
    spectraik210 = models.SmallIntegerField(db_column='spectraIK210', blank=True, null=True)  # Field name made lowercase.
    num_ppi2 = models.SmallIntegerField(blank=True, null=True)
    dlit = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VP01_TehnS'
        unique_together = (('id', 'id_pr'),)


class Vp01Ustmsu(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_ustmsu = models.SmallIntegerField(db_column='num_UstMSU', blank=True, null=True)  # Field name made lowercase.
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    date_n = models.DateTimeField(db_column='date_N', blank=True, null=True)  # Field name made lowercase.
    date_k = models.DateTimeField(db_column='date_K', blank=True, null=True)  # Field name made lowercase.
    complectmsu = models.SmallIntegerField(db_column='complectMSU', blank=True, null=True)  # Field name made lowercase.
    spectravd1 = models.SmallIntegerField(db_column='spectraVD1', blank=True, null=True)  # Field name made lowercase.
    spectravd2 = models.SmallIntegerField(db_column='spectraVD2', blank=True, null=True)  # Field name made lowercase.
    spectravd3 = models.SmallIntegerField(db_column='spectraVD3', blank=True, null=True)  # Field name made lowercase.
    spectraik4 = models.SmallIntegerField(db_column='spectraIK4', blank=True, null=True)  # Field name made lowercase.
    spectraik5 = models.SmallIntegerField(db_column='spectraIK5', blank=True, null=True)  # Field name made lowercase.
    spectraik6 = models.SmallIntegerField(db_column='spectraIK6', blank=True, null=True)  # Field name made lowercase.
    spectraik7 = models.SmallIntegerField(db_column='spectraIK7', blank=True, null=True)  # Field name made lowercase.
    spectraik8 = models.SmallIntegerField(db_column='spectraIK8', blank=True, null=True)  # Field name made lowercase.
    spectraik9 = models.SmallIntegerField(db_column='spectraIK9', blank=True, null=True)  # Field name made lowercase.
    spectraik10 = models.SmallIntegerField(db_column='spectraIK10', blank=True, null=True)  # Field name made lowercase.
    nombe_ppi2 = models.SmallIntegerField(blank=True, null=True)
    dlit = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VP01_UstMSU'
        unique_together = (('id', 'id_pr'),)


class Vp01Vd(models.Model):
    id_pr = models.ForeignKey(Vp01, models.DO_NOTHING, db_column='id_pr')
    num_kalibrvd = models.SmallIntegerField(db_column='num_KalibrVD', blank=True, null=True)  # Field name made lowercase.
    datenach = models.DateTimeField(db_column='dateNach', blank=True, null=True)  # Field name made lowercase.
    datecon = models.DateTimeField(db_column='dateCon', blank=True, null=True)  # Field name made lowercase.
    complectmsu = models.SmallIntegerField(db_column='complectMSU', blank=True, null=True)  # Field name made lowercase.
    num_ppi = models.SmallIntegerField(blank=True, null=True)
    dlit = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VP01_VD'
        unique_together = (('id', 'id_pr'),)


# class AuthGroup(models.Model):
#     name = models.CharField(unique=True, max_length=80)
#
#     class Meta:
#         managed = False
#         db_table = 'auth_group'
#
#
# class AuthGroupPermissions(models.Model):
#     group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
#     permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)
#
#     class Meta:
#         managed = False
#         db_table = 'auth_group_permissions'
#         unique_together = (('group', 'permission'),)
#
#
# class AuthPermission(models.Model):
#     name = models.CharField(max_length=255)
#     content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
#     codename = models.CharField(max_length=100)
#
#     class Meta:
#         managed = False
#         db_table = 'auth_permission'
#         unique_together = (('content_type', 'codename'),)
#
#
# class AuthUser(models.Model):
#     password = models.CharField(max_length=128)
#     last_login = models.DateTimeField(blank=True, null=True)
#     is_superuser = models.BooleanField()
#     username = models.CharField(unique=True, max_length=150)
#     first_name = models.CharField(max_length=30)
#     last_name = models.CharField(max_length=150)
#     email = models.CharField(max_length=254)
#     is_staff = models.BooleanField()
#     is_active = models.BooleanField()
#     date_joined = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'auth_user'
#
#
# class AuthUserGroups(models.Model):
#     user = models.ForeignKey(AuthUser, models.DO_NOTHING)
#     group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
#
#     class Meta:
#         managed = False
#         db_table = 'auth_user_groups'
#         unique_together = (('user', 'group'),)
#
#
# class AuthUserUserPermissions(models.Model):
#     user = models.ForeignKey(AuthUser, models.DO_NOTHING)
#     permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)
#
#     class Meta:
#         managed = False
#         db_table = 'auth_user_user_permissions'
#         unique_together = (('user', 'permission'),)
#
#
# class DjangoAdminLog(models.Model):
#     action_time = models.DateTimeField()
#     object_id = models.TextField(blank=True, null=True)
#     object_repr = models.CharField(max_length=200)
#     action_flag = models.SmallIntegerField()
#     change_message = models.TextField()
#     content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
#     user = models.ForeignKey(AuthUser, models.DO_NOTHING)
#
#     class Meta:
#         managed = False
#         db_table = 'django_admin_log'
#
#
# class DjangoContentType(models.Model):
#     app_label = models.CharField(max_length=100)
#     model = models.CharField(max_length=100)
#
#     class Meta:
#         managed = False
#         db_table = 'django_content_type'
#         unique_together = (('app_label', 'model'),)
#
#
# class DjangoMigrations(models.Model):
#     app = models.CharField(max_length=255)
#     name = models.CharField(max_length=255)
#     applied = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'django_migrations'
#
#
# class DjangoSession(models.Model):
#     session_key = models.CharField(primary_key=True, max_length=40)
#     session_data = models.TextField()
#     expire_date = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'django_session'
