from django.shortcuts import render
from django.views import View
from .models import *
from datetime import datetime, timedelta
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from sys import stderr

# Стандартный формат времени, напр. 2014-01-01
# При изменении нужно поменять images/common_tags и images/views
STD_TIME_FORMAT = "%Y-%m-%d"
# Дата начала и конца выборки по умолчанию -- за последнюю неделю
BEGINNING_DATE = datetime.today() - timedelta(days=7)
ENDING_DATE = datetime.today()

# Все последующие операции проводятся относительно этих двух словарей
# Внести новое поле в FIELDS_TO_REQUEST можно только если оно присутствует во ВСЕХ таблицах из TABLES_W_NAMES.

# FIELDS_TO_REQUEST включает все поля таблиц, которые могут быть видны пользователю, сопоставляя табличное название поля
# с русскоязычным.
FIELDS_TO_REQUEST = {"Дата начала": "datenach", "Дата окончания": "datecon", }
FOREIGN_FIELDS = {"Номер рабочей программы": "n_rp", }
# TABLES_W_NAMES включает все таблицы, которые могут быть видны пользователю, и их русские имена
TABLES_W_NAMES = {"МЦИ": Vp01Mci, "ОМИ": Vp01Omi, "Технологическая съёмка": Vp01Tehns, "МСУ": Vp01Msu, }


def date_is_valid(d, standard_time_format, log=False):
    try:
        datetime.strptime(d, standard_time_format)
        return True
    except (TypeError, ValueError) as e:
        if log:
            print("Error: wrong date format. Correct date format is " + standard_time_format, file=stderr)
        return False


class MultitableView(View):
    template_name = 'satellite_production/multiple_tables_w_form.html'
    beginning_date = BEGINNING_DATE
    ending_date = ENDING_DATE
    requested_tables = TABLES_W_NAMES

    # Сначала проверка разрешений, если она будет необходима
    def get(self, request):

        # Фильтры из url-запроса
        self.beginning_date, self.ending_date = self.filter_date(request.GET.get("bdate"), request.GET.get("edate"), STD_TIME_FORMAT)
        checkbox_tables = request.GET.getlist('checkbox-tables')

        if checkbox_tables:
            self.requested_tables = {k: v for (k, v) in self.requested_tables.items() if k in checkbox_tables}

        dict_requested = self.make_query(self.beginning_date, self.ending_date, FIELDS_TO_REQUEST, FOREIGN_FIELDS)
        request_empty = True
        for v in dict_requested.values():
            if v:
                request_empty = False
                break

        p = Paginator(tuple(dict_requested.items()), 1)
        page = request.GET.get('page')
        try:
            p_display = p.get_page(page)
        except PageNotAnInteger:
            p_display = p.get_page(1)
        except EmptyPage:
            p_display = p.get_page(p.num_pages)

        context = {
            'all_tables': p_display,
            'request_empty': request_empty,
            'beg_date': self.beginning_date,
            'end_date': self.ending_date,
            'table_names': self.requested_tables,
            'checkboxes': TABLES_W_NAMES.keys(),
            'field_names': dict(FOREIGN_FIELDS, **FIELDS_TO_REQUEST).keys(),
        }
        return render(request, self.template_name, context)

    # Если дата начала или дата окончания не были предоставлены, присваивает соответствующей переменной значение,
    # заданное параметром time_span (по умолчанию неделя)
    @staticmethod
    def filter_date(bdate: str, edate: str, time_format: str, time_span=timedelta(days=7)) -> (datetime, datetime):
        # Значения по умолчанию
        new_beginning_date = datetime.today()
        new_ending_date = ""

        # Валидация даты
        if date_is_valid(bdate, time_format):
            new_beginning_date = datetime.strptime(bdate, time_format)
        if date_is_valid(edate, time_format):
            new_ending_date = datetime.strptime(edate, time_format)
        else:
            new_ending_date = new_beginning_date + time_span

        if new_beginning_date >= new_ending_date:
            new_beginning_date = new_ending_date - time_span
        return new_beginning_date, new_ending_date

    # Вспомогательная функция, возвращающая словарь, ключи которого - имена таблиц, а значения - результаты запроса
    # Результат запроса представлен из списка кортежей для всех записей таблицы, прошедших фильтр.
    # Кортежи представлены следующим образом: сначала поля FOREIGN_FIELDS, затем поля FIELDS_TO_REQUEST
    def make_query(self, bdate:datetime, edate:datetime, table_fields, foreign_fields) -> {}:
        dict_requested = {}

        for tname, table in self.requested_tables.items():
            # Предварительная загрузка полей из Vp01 и фильтр по дате
            filtered = table.objects.filter(datenach__gte=bdate,
                                            datecon__lte=edate) \
                .select_related()

            # Фильтрация необходимых полей через просмотр свойств каждой записи, затем преобразование в кортеж
            # Подразумевается, что вторичный ключ -- id_pr, а список необходимых полей -- значения foreign_fields
            foreign_fields_list = []
            for record in filtered:
                record_fields_list = []
                for field in foreign_fields.values():
                    for k, v in record.id_pr.__dict__.items():
                        if k == field:
                            record_fields_list.append(v)
                            break
                foreign_fields_list.append(tuple(record_fields_list))

            table_list = [(*rec[0], *rec[1]) for rec in zip(foreign_fields_list, filtered.values_list(*table_fields.values()))]
            dict_requested[tname] = table_list

        return dict_requested



