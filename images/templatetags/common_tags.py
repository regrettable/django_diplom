from django import template
from diplom.settings import FOLDER_FILTER_FORMAT, DATETIME_FORMAT

register = template.Library()


@register.simple_tag()
def update_variable(v):
    return v


# Возвращает элемент списка или словаря, если он существует, иначе False
@register.simple_tag()
def get_dict_value(d, v):
    result = False
    try:
        result = d[v]
    except KeyError:
        pass
    return result


# На входе - путь к изображению, на выходе - словарь, возможные ключи которого:
# year, month, day, number, filename, time
@register.simple_tag()
def get_date_from_path(path: str, **kwargs) -> []:
    p_format = kwargs.get("path_format", FOLDER_FILTER_FORMAT + "/%n/%f").split("/")
    path_list = path.split("/")[1:]

    final_dict = {}
    for format_tag, item in zip(p_format, path_list):
        if format_tag == "%Y":
            final_dict["year"] = item
        elif format_tag == "%m":
            final_dict["month"] = item
        elif format_tag == "%d":
            final_dict["day"] = item
        elif format_tag == "%n":
            final_dict["number"] = item
        elif format_tag == "%f":
            final_dict["filename"] = item
        elif format_tag == "%t":
            final_dict["time"] = item
    return final_dict
