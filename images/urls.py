from .views import *
from django.urls import path

app_name = 'images'
urlpatterns = [
    path(r'', ImageView.as_view(), name='image_view'),
]
