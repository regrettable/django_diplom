from django.shortcuts import render
from django.views import View
from diplom.settings import STATICFILES_DIRS, FOLDER_FILTER_FORMAT
from satellite_production.views import STD_TIME_FORMAT, date_is_valid
from datetime import datetime, timedelta
from os import path as ospath
from os import walk,listdir

IMAGES_PATH = STATICFILES_DIRS[0]
SESSION_FORMAT = "%H%M"
MONTHS_RU = {
    "01": "Январь",
    "02": "Февраль",
    "03": "Март",
    "04": "Апрель",
    "05": "Май",
    "06": "Июнь",
    "07": "Июль",
    "08": "Август",
    "09": "Сентябрь",
    "10": "Октябрь",
    "11": "Ноябрь",
    "12": "Декабрь",
}

MONTHS_EN = {
    "01": "January",
    "02": "February",
    "03": "March",
    "04": "April",
    "05": "May",
    "06": "June",
    "07": "July",
    "08": "August",
    "09": "September",
    "10": "October",
    "11": "November",
    "12": "December",
}

# Возвращает все изображения по заданному пути, следуя во вложенные папки.
# Можно указать форматы при помощи formats= через пробел.
def get_images_urls(root_path: str, path_to_images: str, **kwargs) -> [str]:
        # Перевод форматов в форму кортежа
        image_formats = tuple(kwargs.get('formats', ".png .jpg").split(" "))
        file_paths = []
        for root, dirs, files in walk(ospath.abspath(path_to_images)):
            for f in files:
                if f.endswith(image_formats):
                    relative_path = ospath.join(root, f).replace(root_path, "")
                    file_paths.append(relative_path)
        return file_paths


# Важно: если формат даты поменяется, здесь его тоже нужно будет поменять
def get_images_folder(root_path: str, folder_format: str, date, session: str) -> str:
    # Убираем лишние слеши в пути
    if root_path[-1:] != "/" or root_path[-1:] != "\\":
        root_path = root_path + "/"
    if folder_format[-1:] == "/" or folder_format[-1:] == "\\":
        folder_format = folder_format[:-1]
    folder_format += "/%n"

    # Год, месяц, день, номер съёмки в этом порядке
    images_folder_list = ['', '', '', '']
    folder_format_list = folder_format.split("/")
    for arg in folder_format_list:
        if arg == "%Y":
            images_folder_list[0] = str(date.year)
        elif arg == "%m":
            images_folder_list[1] = str(date.month).zfill(2)
        # ВНИМАНИЕ: Зависит от системной локали
        elif arg == "%B":
            images_folder_list[1] = MONTHS_EN[str(date.month).zfill(2)]
        elif arg == "%d":
            images_folder_list[2] = str(date.day).zfill(2)
        elif arg == "%n":
            images_folder_list[3] = session
    if session == '':
        images_folder_list = images_folder_list[:-1]
    return root_path + "/".join(images_folder_list)


# Возвращает все папки в данной папке, используется для нахождения номеров сессий за данный день
def get_all_folders(path: str) -> [str]:
    return [folder for folder in listdir(path) if ospath.isdir(ospath.join(path, folder))]


def session_is_valid(session: str, session_format: str) -> bool:
    try:
        datetime.strptime(session, session_format)
        return True
    except ValueError:
        return False


class ImageView(View):
    template_name = 'images/images.html'
    context = {
        # По умолчанию запрашивать изображение за вчерашний день
        "date": datetime.today() - timedelta(1),
        "image_list": [],
        # Для функций шаболонов из templatetags/common_tags
        "range_list": [],
        "current_session": '',
        "session_list": [],
        "nothing_to_show": False,
        "input_date": '',
    }

    def get(self, request):
        # Дата из запроса
        image_date = request.GET.get('sdate')
        if image_date and date_is_valid(image_date, STD_TIME_FORMAT):
            self.context["date"] = datetime.strptime(image_date, STD_TIME_FORMAT)
        session = request.GET.get('session')
        try:
            session_list = get_all_folders(IMAGES_PATH + '/' + self.context['date'].strftime(FOLDER_FILTER_FORMAT))
            self.context['session_list'] = session_list

            # Если номер сессии не указан или в неправильном формате, выбрать первую сессию за день
            if session and session_is_valid(session, SESSION_FORMAT):
                self.context['current_session'] = session
            else:
                self.context['current_session'] = session_list[0]

            self.context["image_list"] = get_images_urls(IMAGES_PATH, get_images_folder(IMAGES_PATH, FOLDER_FILTER_FORMAT, self.context["date"], self.context['current_session']))

            self.context["range_list"] = [self.context["image_list"].index(i) for i in self.context["image_list"]]
            self.context['nothing_to_show'] = False
        except (FileNotFoundError, IndexError) as e:
            self.context['nothing_to_show'] = True
        self.context['input_date'] = self.context['date'].strftime(STD_TIME_FORMAT)
        return render(request, self.template_name, self.context)
