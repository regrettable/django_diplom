FROM python:3.6.5

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apt-get update && apt-get install -y --no-install-recommends apt-transport-https \
    apt-utils \
    build-essential \
    curl \
    gcc \
    wget \
    vim

RUN         curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN         curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list

RUN apt-get update && ACCEPT_EULA=Y apt-get install -y --no-install-recommends unixodbc-dev \
    mssql-tools


RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
