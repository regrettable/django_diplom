"""diplom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from satellite_production import urls as prod_urls
from diplom_base import urls as base_urls
from django.views.generic.base import RedirectView

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'images/', include('images.urls')),
    path(r'production/', include('satellite_production.urls')),
    # Стандартные виды для главной страницы и др.
    path(r'', include('diplom_base.urls')),
    # Стандартное перенаправление
    path(r'.+', RedirectView.as_view(url="/")),
]
