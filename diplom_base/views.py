from django.views.generic.base import TemplateView
# Create your views here.


# Простой рендер шаблона главной страницы
class HomePageView(TemplateView):
    template_name = 'diplom_base/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Дополнительные параметры идут сюда

        return context


# Простой рендер шаблона страницы "о сайте"
class AboutView(TemplateView):
    template_name = 'diplom_base/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Дополнительные параметры идут сюда

        return context
