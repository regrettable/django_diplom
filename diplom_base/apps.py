from django.apps import AppConfig


class DiplomBaseConfig(AppConfig):
    name = 'diplom_base'
