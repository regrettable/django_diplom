from .views import *
from django.urls import path

app_name = 'diplom_base'
urlpatterns = [
    path(r'about', AboutView.as_view(), name='about'),
    path(r'', HomePageView.as_view(), name='home'),
]
